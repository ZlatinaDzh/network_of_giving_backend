package com.example.demo.impl;
import com.example.demo.data.Address;
import com.example.demo.data.Gender;
import com.example.demo.data.User;
import com.example.demo.interfaces.IRepository;
import com.example.demo.interfaces.UserManagerI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserManager implements UserManagerI {
    private final IRepository repository;
    private final AddressManager addressManager;
    @Autowired
    public UserManager(IRepository repository, AddressManager addressManager){
        this.repository = repository;
        this.addressManager = addressManager;
    }

    @Override
    public User getUser(String name) {
        return this.repository.getUser(name);
    }

    @Override
    public boolean addUser(String firstName, String lastName, String userName, String password, int age, Gender gender,
                        String country, String town, String neighbourhood, String street, int apartmentNumber) {
        if(this.checkIfUserExist(userName)) {
            System.out.println("Here");
           throw new IllegalArgumentException("Username already exists!");
        }
        int idAddress = addressManager.addAddress(new Address(0, country, town, neighbourhood, street, apartmentNumber));
        this.repository.addUser(new User(firstName, lastName, userName, password, age, gender, idAddress));
        return true;
    }


    @Override
    public List<User> getAllUsers() {
        return this.repository.getAllUsers();
    }

    @Override
    public boolean authentication(String userName, String password) {
        return this.repository.authentication(userName,password);
    }

    @Override
    public boolean checkIfUserExist(String userName) {
        return this.repository.checkIfUserExist(userName);
    }

}
