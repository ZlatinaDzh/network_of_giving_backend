package com.example.demo.impl;

import com.example.demo.data.*;
import com.example.demo.interfaces.IRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class JDBCRepository implements IRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public JDBCRepository(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public Address getAddress(int idAddress) {
        return this.jdbcTemplate.queryForObject("select * from address where id = ?", this::AddressRowMapper, idAddress);
    }

    @Override
    public Address getAddress(String country, String town, String neighbourhood, String street, int apartmentNumber) {
        return this.jdbcTemplate.queryForObject(
                ("SELECT * FROM address " +
                        "WHERE country=? " +
                        "AND town=? " +
                        "AND neighbourhood=? " +
                        "AND street=? " +
                        "AND apartment_number=?;"),
                this::AddressRowMapper,
                country,town,neighbourhood,street,apartmentNumber);
    }

    @Override
    public int addAddress(Address address) {
        jdbcTemplate.update("INSERT INTO address (country,town,neighbourhood,street,apartment_number) VALUES (?, ?, ?, ?, ?)",
                address.getCountry(),address.getTown(),address.getNeighbourhood(), address.getStreet(),address.getApartmentNumber());
        List<Integer> result = jdbcTemplate.queryForList("SELECT id FROM address WHERE country=? AND town=? AND neighbourhood=? AND street=? AND apartment_number=?",
                                  Integer.class,  address.getCountry(),address.getTown(),address.getNeighbourhood(), address.getStreet(),address.getApartmentNumber());
        return result.get(0);
    }

    @Override
    public void updateAddress(int idAddress, Address address) {
        jdbcTemplate.update("UPDATE address SET country = ?, town = ?, neighbourhood = ?, street = ?, apartment_number = ? WHERE id = ?",
                address.getCountry(),address.getTown(), address.getNeighbourhood(), address.getStreet(),address.getApartmentNumber(),idAddress);

    }

    @Override
    public void deleteAddress(int idAddress) {
        jdbcTemplate.update("DELETE FROM address WHERE id = ? ", idAddress);
    }

    @Override
    public List<Address> getAllAddresses() {
        return this.jdbcTemplate.query("select * from address", this::AddressRowMapper);
    }

    @Override
    public boolean checkIfAddressExist(String country, String town, String neighbourhood, String street, int apartmentNumber) {
        return !(jdbcTemplate.query("SELECT * FROM address WHERE country=? AND town=? AND neighbourhood=? AND street=? AND apartment_number=?",
                this::AddressRowMapper, country, town, neighbourhood, street,apartmentNumber)).isEmpty();
    }

    @Override
    public User getUser(String name) {
        return this.jdbcTemplate.queryForObject("SELECT * FROM users WHERE user_name = ?", this::UserRowMapper, name);
    }

    @Override
    public void addUser(User user) {
        this.jdbcTemplate.update("INSERT INTO users (first_name, last_name, user_name, u_password, age, gender, addr_id) VALUES (?,?, ?, ?, ?, ?,?)",
                user.getFirstName(),user.getLastName(),user.getUsername(),user.getPassword(),user.getAge(),user.getGender(),user.getIdAddress());
    }

    @Override
    public List<User> getAllUsers() {
        return this.jdbcTemplate.query("select * from users", this::UserRowMapper);
    }

    @Override
    public boolean checkIfUserExist(String userName) {
        return !(jdbcTemplate.query("SELECT * FROM users WHERE user_name = ?", this::UserRowMapper, userName)).isEmpty();
    }

    @Override
    public boolean authentication(String userName, String password) {
        User getUser = jdbcTemplate.queryForObject("SELECT * FROM users WHERE user_name = ?", this::UserRowMapper, userName);
        return getUser.getPassword().equals(password);
    }

    @Override
    public Charity getCharity(String nameCharity) {
        return this.jdbcTemplate.queryForObject("SELECT * FROM charity WHERE c_name = ?", this::CharityRowMapper, nameCharity);
    }

    @Override
    public void addCharity(Charity charity) {
        this.jdbcTemplate.update("INSERT INTO charity (c_name,thumbnail,c_description,number_of_participants,total_budget_required, creator_name) VALUES (?, ?, ?, ?, ?,?)",
                charity.getName(), charity.getThumbnail(), charity.getDescription(), charity.getNumberOfParticipants(),
                charity.getRequiredBudget(), charity.getNameCreator());
    }

    @Override
    public void updateCharity(String nameCharity, Charity charity) {

    }

    @Override
    public void deleteCharity(String nameCharity) {
        System.out.println("delete charity");
        jdbcTemplate.update("DELETE FROM charity WHERE c_name = ? ", nameCharity);
    }

    @Override
    public List<Charity> getAllCharities() {
        return this.jdbcTemplate.query("select * from charity", this::CharityRowMapper);
    }

    @Override
    public boolean checkIfCharityExist(String charityName) {
        return !(jdbcTemplate.query("SELECT * FROM charity WHERE c_name= ?", this::CharityRowMapper, charityName)).isEmpty();
    }


    @Override
    public void addDonation(Donation donation) {
        this.jdbcTemplate.update("INSERT INTO donations (name_donator,name_charity,amount,donation_time) VALUES (?, ?, ?, ?)",
                donation.getNameDonator(),donation.getNameCharity(),donation.getAmount(), MyDate.getDateTimeAsString());
    }

    @Override
    public List<Donation> getAllDonationsByUser(String userName) {
        return null;
    }

    @Override
    public List<Donation> getAllDonationsByCharity(String nameCharity) {
        return this.jdbcTemplate.query("SELECT * from donations WHERE name_charity=?", this::DonationRowMapper, nameCharity);
    }

    @Override
    public int getCollectedAmountForCharity(String nameCharity) {
        Integer result = jdbcTemplate.queryForObject("SELECT sum(amount) from donations WHERE name_charity=? ", Integer.class, nameCharity);
        return (result != null) ? result : 0;
    }


    @Override
    public void addParticipation(Participation participation) {
        this.jdbcTemplate.update("INSERT INTO participants (name_participant,name_charity,donation_time) VALUES (?, ?, ?)",
                participation.getNameParticipant(), participation.getNameCharity(), MyDate.getDateTimeAsString());
    }


    @Override
    public int getCollectedParticipationsForCharity(String nameCharity) {
        Integer result = jdbcTemplate.queryForObject("SELECT count(id) from participants WHERE name_charity=? ", Integer.class, nameCharity);
        return (result != null) ? result : 0;
    }


    @Override
    public boolean checkUserIsParticipantForCharity(String username, String charityName) {
        return !(jdbcTemplate.query("SELECT * FROM participants WHERE (name_participant = ? AND name_charity = ?)",
                this::ParticipationRowMapper, username, charityName)).isEmpty();
    }

    private Address AddressRowMapper(ResultSet resultSet, int rowNum) throws SQLException {
        return new Address(
                resultSet.getInt("id"),
                resultSet.getString("country"),
                resultSet.getString("town"),
                resultSet.getString("neighbourhood"),
                resultSet.getString("street"),
                resultSet.getInt("apartment_number")
        );
    }

    private User UserRowMapper(ResultSet resultSet, int rowNum) throws SQLException {
        Gender gender;
        String genderDB = resultSet.getString("gender");
        if (genderDB == "male") {
            gender = Gender.MALE;
        } else if (genderDB == "female") {
            gender = Gender.FEMALE;
        } else {
            gender = Gender.OTHER;
        }
        return new User(
                resultSet.getString("first_name"),
                resultSet.getString("last_name"),
                resultSet.getString("user_name"),
                resultSet.getString("u_password"),
                resultSet.getInt("age"),
                gender,
                resultSet.getInt("addr_id")
        );
    }

    private Charity CharityRowMapper(ResultSet resultSet, int rowNum) throws SQLException {
        return new Charity(
                resultSet.getString("c_name"),
                resultSet.getString("thumbnail"),
                resultSet.getString("c_description"),
                resultSet.getInt("number_of_participants"),
                resultSet.getInt("total_budget_required"),
                resultSet.getString("creator_name")
        );
    }

    private Donation DonationRowMapper(ResultSet resultSet, int rowNum) throws SQLException {
        return new Donation(
                resultSet.getInt("id"),
                resultSet.getString("name_donator"),
                resultSet.getString("name_charity"),
                resultSet.getInt("amount"),
                resultSet.getString("donation_time")
        );
    }

    private Participation ParticipationRowMapper(ResultSet resultSet, int rowNum) throws SQLException {
        return new Participation(
                resultSet.getInt("id"),
                resultSet.getString("name_participant"),
                resultSet.getString("name_charity"),
                resultSet.getString("donation_time")
        );
    }
}
