package com.example.demo.impl;

import com.example.demo.data.Address;
import com.example.demo.interfaces.AddressManagerI;
import com.example.demo.interfaces.IRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AddressManager implements AddressManagerI {

    private final IRepository repository;

    @Autowired
    public AddressManager(IRepository repository) {
        this.repository = repository;
    }

    @Override
    public Address getAddress(int idAddress) {
        return repository.getAddress(idAddress);
    }

    @Override
    public int addAddress(Address address) {
        if(this.checkIfAddressExist(address.getCountry(),address.getTown(),address.getNeighbourhood(),
                address.getStreet(),address.getApartmentNumber())){
            return this.repository.getAddress(address.getCountry(), address.getTown(), address.getNeighbourhood(),
                    address.getStreet(), address.getApartmentNumber()).getId();
        }
        return repository.addAddress(address);
    }

    @Override
    public void updateAddress(int idAddress, Address address) {
        repository.updateAddress(idAddress, address);
    }

    @Override
    public void deleteAddress(int idAddress) {
        repository.deleteAddress(idAddress);
    }

    @Override
    public List<Address> getAllAddresses() {
        return repository.getAllAddresses();
    }

    @Override
    public boolean checkIfAddressExist(String country, String town, String neighbourhood, String street, int apartmentNumber) {
        return this.repository.checkIfAddressExist(country, town, neighbourhood, street,apartmentNumber);
    }
}
