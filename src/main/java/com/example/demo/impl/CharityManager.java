package com.example.demo.impl;
import com.example.demo.data.Charity;
import com.example.demo.interfaces.CharityManagerI;
import com.example.demo.interfaces.IRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CharityManager implements CharityManagerI {
    private final IRepository repository;

    @Autowired
    public CharityManager(IRepository repository) {
        this.repository = repository;
    }

    @Override
    public Charity getCharity(String nameCharity) {
        return this.repository.getCharity(nameCharity);
    }

    @Override
    public boolean addCharity(Charity charity) {
        if(this.checkIfCharityExist(charity.getName())) {
            throw new IllegalArgumentException("Charity name already exists!");
        }

        this.repository.addCharity(charity);
        return true;
    }

    @Override
    public void updateCharity(String nameCharity, Charity charity) {
        this.repository.updateCharity(nameCharity, charity);
    }

    @Override
    public void deleteCharity(String nameCharity) {
        this.repository.deleteCharity(nameCharity);
    }

    @Override
    public List<Charity> getAllCharities() {
        return this.repository.getAllCharities();
    }

    @Override
    public boolean checkIfCharityExist(String charityName) {
        return this.repository.checkIfCharityExist(charityName);
    }

}
