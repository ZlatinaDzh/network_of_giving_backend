package com.example.demo.impl;

import com.example.demo.data.Donation;
import com.example.demo.interfaces.DonationManagerI;
import com.example.demo.interfaces.IRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DonationManager implements DonationManagerI {
    private final IRepository repository;

    @Autowired
    public DonationManager(IRepository repository){
        this.repository = repository;
    }

    @Override
    public void addDonation(Donation donation) {
        this.repository.addDonation(donation);
    }

    @Override
    public List<Donation> getAllDonationsByUser(String userName) {
        return this.repository.getAllDonationsByUser(userName);
    }

    @Override
    public List<Donation> getAllDonationsByCharity(String nameCharity) {
        return this.repository.getAllDonationsByCharity(nameCharity);
    }

    @Override
    public int getCollectedAmountForCharity(String nameCharity) {
        return this.repository.getCollectedAmountForCharity(nameCharity);
    }

}
