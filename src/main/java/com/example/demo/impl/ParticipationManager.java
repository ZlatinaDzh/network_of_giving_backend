package com.example.demo.impl;

import com.example.demo.data.Participation;
import com.example.demo.interfaces.IRepository;
import com.example.demo.interfaces.ParticipationManagerI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ParticipationManager implements ParticipationManagerI {

    @Autowired
    private final IRepository repository;

    public ParticipationManager(IRepository repository){
        this.repository = repository;
    }

    @Override
    public void addParticipation(Participation participation) {
         this.repository.addParticipation(participation);
    }


    @Override
    public int getCollectedParticipationsForCharity(String nameCharity) {
        return this.repository.getCollectedParticipationsForCharity(nameCharity);
    }

    @Override
    public boolean checkUserIsParticipantForCharity(String username, String charityName) {
        return this.repository.checkUserIsParticipantForCharity(username, charityName);
    }
}
