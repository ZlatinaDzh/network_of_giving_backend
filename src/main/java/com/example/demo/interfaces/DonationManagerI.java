package com.example.demo.interfaces;
import com.example.demo.data.Donation;

import java.util.List;

public interface DonationManagerI {
    void addDonation(Donation donation);
    List<Donation> getAllDonationsByUser(String userName);
    List<Donation> getAllDonationsByCharity(String nameCharity);
    int getCollectedAmountForCharity(String nameCharity);
}
