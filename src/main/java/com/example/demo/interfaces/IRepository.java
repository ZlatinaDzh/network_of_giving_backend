package com.example.demo.interfaces;


import com.example.demo.data.*;

import java.util.List;

public interface IRepository {

    //Address
    Address getAddress(int idAddress);
    Address getAddress(String country, String town, String neighbourhood, String street, int apartmentNumber);
    int addAddress(Address address);
    void updateAddress(int idAddress, Address address);
    void deleteAddress(int idAddress);
    List<Address> getAllAddresses();
    boolean checkIfAddressExist(String country, String town, String neighbourhood, String street, int apartmentNumber);

    //User
    User getUser(String name);
    void addUser(User user);
    List<User> getAllUsers();
    boolean checkIfUserExist(String userName);
    boolean authentication(String userName, String password);

    //Charity
    Charity getCharity(String nameCharity);
    void addCharity(Charity charity);
    void updateCharity(String nameCharity, Charity charity);
    void deleteCharity(String nameCharity);
    List<Charity> getAllCharities();
    boolean checkIfCharityExist(String charityName);

    //Donation
    void addDonation(Donation donation);
    List<Donation> getAllDonationsByUser(String userName);
    List<Donation> getAllDonationsByCharity(String nameCharity);
    int getCollectedAmountForCharity(String nameCharity);

    //Participation
    void addParticipation(Participation participation);
    int getCollectedParticipationsForCharity(String nameCharity);
    boolean checkUserIsParticipantForCharity(String username, String charityName);

}
