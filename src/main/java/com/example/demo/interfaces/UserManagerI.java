package com.example.demo.interfaces;

import com.example.demo.data.Gender;
import com.example.demo.data.User;

import java.util.List;

public interface UserManagerI {
    User getUser(String name);
    boolean addUser(String firstName, String lastName, String userName, String password, int age, Gender gender,
                    String country, String town, String neighbourhood, String street, int apartmentNumber);
    List<User> getAllUsers();
    boolean authentication(String userName, String password);
    boolean checkIfUserExist(String userName);
}
