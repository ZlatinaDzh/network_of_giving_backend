package com.example.demo.interfaces;

import com.example.demo.data.Charity;

import java.util.List;

public interface CharityManagerI {
    Charity getCharity(String nameCharity);
    boolean addCharity(Charity charity);
    void updateCharity(String nameCharity, Charity charity);
    void deleteCharity(String nameCharity);
    List<Charity> getAllCharities();
    boolean checkIfCharityExist(String charityName);
}
