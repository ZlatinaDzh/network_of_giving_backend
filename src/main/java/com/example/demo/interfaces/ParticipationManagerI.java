package com.example.demo.interfaces;
import com.example.demo.data.Participation;

public interface ParticipationManagerI {
    void addParticipation(Participation participation);
    int getCollectedParticipationsForCharity(String nameCharity);
    boolean checkUserIsParticipantForCharity(String username, String charityName);
}
