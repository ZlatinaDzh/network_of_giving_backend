package com.example.demo.interfaces;

import com.example.demo.data.Address;

import java.util.List;

public interface AddressManagerI {
    Address getAddress(int id);
    int addAddress(Address address);
    void updateAddress(int idAddress, Address address);
    void deleteAddress(int idAddress);
    List<Address> getAllAddresses();
    boolean checkIfAddressExist(String country, String town, String neighbourhood, String street, int apartmentNumber);
}
