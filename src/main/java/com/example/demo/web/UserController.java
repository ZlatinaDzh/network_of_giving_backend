package com.example.demo.web;

import com.example.demo.data.User;
import com.example.demo.impl.UserManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    private final UserManager userManager;

    public UserController(UserManager userManager) {
        this.userManager = userManager;
    }

    @GetMapping
    public List<User> getAllUsers() {
        return this.userManager.getAllUsers();
    }

    @GetMapping("/{userName}")
    public User getUser(@PathVariable("userName") String userName) {
        return this.userManager.getUser(userName);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public boolean addUser(@RequestBody @Valid UserDTO user) {
        try {
            return this.userManager.addUser(user.getFirstName(), user.getLastName(), user.getUserName(), user.getPassword(),
                    user.getAge(), user.getGender(), user.getCountry(), user.getTown(),
                    user.getNeighbourhood(), user.getStreet(), user.getApartmentNumber());
        } catch (IllegalArgumentException exc) {
        throw new ResponseStatusException(
                HttpStatus.CONFLICT, exc.getMessage(), exc);
        }
    }

    @PostMapping(path = "/login", consumes = MediaType.APPLICATION_JSON_VALUE)
    public boolean authentication (@RequestBody Credentials credentials){
        return this.userManager.authentication(credentials.getFirstAttribute(),credentials.getSecondAttribute());
    }

    @PostMapping(path = "/checkUsername", consumes = MediaType.APPLICATION_JSON_VALUE)
    public boolean checkIfUserExist(@RequestBody String username){
        return this.userManager.checkIfUserExist(username);
    }
}
