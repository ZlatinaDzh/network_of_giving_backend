package com.example.demo.web;

import com.example.demo.data.Address;
import com.example.demo.impl.AddressManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/addresses", produces = MediaType.APPLICATION_JSON_VALUE)
public class AddressController {
    private final AddressManager addressManager;


    public AddressController(AddressManager addressManager){
        this.addressManager = addressManager;
    }

    @GetMapping
    public List<Address> getAllAddresses() {
        return this.addressManager.getAllAddresses();
    }

    @GetMapping("/{id}")
    public Address getAddress(@PathVariable("id") int id) {
        return this.addressManager.getAddress(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public int addAddress(@RequestBody @Valid Address address) {
        return this.addressManager.addAddress(address);
    }

    @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateAddress(@PathVariable("id") int id, @RequestBody @Valid Address address) {
        this.addressManager.updateAddress(id, address);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAddress(@PathVariable("id") int id) {
        this.addressManager.deleteAddress(id);
    }
}
