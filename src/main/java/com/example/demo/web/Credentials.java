package com.example.demo.web;

public class Credentials {
    private String firstAttribute;
    private String secondAttribute;

    public Credentials() {
    }

    public Credentials(String username, String password) {
        this.firstAttribute = username;
        this.secondAttribute = password;
    }

    public String getFirstAttribute() {
        return firstAttribute;
    }

    public void setFirstAttribute(String firstAttribute) {
        this.firstAttribute = firstAttribute;
    }

    public String getSecondAttribute() {
        return secondAttribute;
    }

    public void setSecondAttribute(String secondAttribute) {
        this.secondAttribute = secondAttribute;
    }
}
