package com.example.demo.web;

import com.example.demo.data.Donation;
import com.example.demo.impl.DonationManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/donations", produces = MediaType.APPLICATION_JSON_VALUE)
public class DonationController {
    private final DonationManager donationManager;

    public DonationController(DonationManager donationManager) {
        this.donationManager= donationManager;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void addDonation(@RequestBody @Valid Donation donation) {
        this.donationManager.addDonation(donation);
    }

    @GetMapping("/donators/{userName}")
    public List<Donation> getAllDonationsByUser(@PathVariable("userName") String userName) {
        return this.donationManager.getAllDonationsByUser(userName);
    }

    @GetMapping("/charities/{nameCharity}")
    public List<Donation> getAllDonationsByCharity(@PathVariable("nameCharity") String nameCharity) {
        return this.donationManager.getAllDonationsByCharity(nameCharity);
    }

    @GetMapping("/for/{nameCharity}")
    public Integer getCollectedAmountForCharity(@PathVariable("nameCharity") String nameCharity) {
        return this.donationManager.getCollectedAmountForCharity(nameCharity);
    }
}
