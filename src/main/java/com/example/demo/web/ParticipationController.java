package com.example.demo.web;

import com.example.demo.data.Participation;
import com.example.demo.impl.ParticipationManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/participations", produces = MediaType.APPLICATION_JSON_VALUE)
public class ParticipationController {
    private final ParticipationManager participationManager;

    public ParticipationController(ParticipationManager participationManager) {
        this.participationManager= participationManager;
    }


    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void addParticipation(@RequestBody @Valid Participation participation) {
        this.participationManager.addParticipation(participation);
    }


    @GetMapping("/for/{nameCharity}")
    public int getCollectedParticipationsForCharity(@PathVariable("nameCharity") String nameCharity) {
        return this.participationManager.getCollectedParticipationsForCharity(nameCharity);
    }

    @PostMapping(path = "/checkParticipant", consumes = MediaType.APPLICATION_JSON_VALUE)
    public boolean checkUserIsParticipantForCharity(@RequestBody Credentials credentials) {
        return this.participationManager.checkUserIsParticipantForCharity(credentials.getFirstAttribute(),credentials.getSecondAttribute());
    }
}
