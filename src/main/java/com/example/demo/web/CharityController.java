package com.example.demo.web;

import com.example.demo.data.Charity;
import com.example.demo.impl.CharityManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/charities", produces = MediaType.APPLICATION_JSON_VALUE)
public class CharityController {

    private final CharityManager charityManager;

    public CharityController(CharityManager charityManager){
        this.charityManager = charityManager;
    }

    @GetMapping
    public List<Charity> getAllCharities() {
        return this.charityManager.getAllCharities();
    }

    @GetMapping("/{charityName}")
    public Charity getCharity(@PathVariable("charityName") String charityName) {
        return this.charityManager.getCharity(charityName);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public boolean addCharity(@RequestBody @Valid Charity charity) {
        try {
            return this.charityManager.addCharity(charity);
        }catch (IllegalArgumentException exc) {
        throw new ResponseStatusException(
                HttpStatus.CONFLICT, exc.getMessage(), exc);
        }
    }

    @PutMapping(path = "/{charityName}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCharity(@PathVariable("charityName") String charityName, @RequestBody @Valid Charity charity) {
        this.charityManager.updateCharity(charityName, charity);
    }

    @DeleteMapping("/{charityName}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCharity(@PathVariable("charityName") String charityName) {
        this.charityManager.deleteCharity(charityName);
    }

    @PostMapping(path = "/checkCharityName", consumes = MediaType.APPLICATION_JSON_VALUE)
    public boolean checkIfCharityExist(@RequestBody String charityName){
        return this.charityManager.checkIfCharityExist(charityName);
    }
}
