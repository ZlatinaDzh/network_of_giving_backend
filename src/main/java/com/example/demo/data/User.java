package com.example.demo.data;

public class User {
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private int age;
    private Gender gender;
    private int idAddress;


    public User(String firstName, String lastName, String userName, String password, int age, Gender gender, int idAddress) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = userName;
        this.password = password;
        this.age = age;
        this.gender = gender;
        this.idAddress = idAddress;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return this.username;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        if(this.gender == Gender.FEMALE){
            return "FEMALE";
        }else if(this.gender == Gender.MALE){
            return "MALE";
        }else{
            return "OTHER";
        }
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getIdAddress() {
        return this.idAddress;
    }

    public void setIdAddress(int idAddress) {
        this.idAddress = idAddress;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userName='" + username + '\'' +
                ", password='" + password + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                ", idAddress=" + idAddress +
                '}';
    }
}
