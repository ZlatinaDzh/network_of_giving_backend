package com.example.demo.data;

import javax.validation.constraints.NotNull;

public class Participation {
    private int id;
    @NotNull
    private String nameParticipant;
    @NotNull
    private String nameCharity;
    private String date;

    public Participation(int id, String nameParticipant, String nameCharity, String date) {
        this.id = id;
        this.nameParticipant = nameParticipant;
        this.nameCharity = nameCharity;
        this.date = date;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameParticipant() {
        return this.nameParticipant;
    }

    public void setNameParticipant(String nameParticipant) {
        this.nameParticipant = nameParticipant;
    }

    public String getNameCharity() {
        return this.nameCharity;
    }

    public void setNameCharity(String nameCharity) {
        this.nameCharity = nameCharity;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Participation{" +
                "id=" + id +
                ", nameParticipant='" + nameParticipant + '\'' +
                ", nameCharity='" + nameCharity + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
