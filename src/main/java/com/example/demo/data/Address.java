package com.example.demo.data;

import javax.validation.constraints.NotNull;

public class Address {
    private int id;
    @NotNull
    private String country;
    private String town;
    private String neighbourhood;
    private String street;
    private int apartmentNumber;

    public Address(int id, String country, String town, String neighbourhood, String street, int apartmentNumber) {
        this.id = id;
        this.country = country;
        this.town = town;
        this.neighbourhood = neighbourhood;
        this.street = street;
        this.apartmentNumber = apartmentNumber;
    }

    public int getId() {
        return this.id;
    }

    public String getCountry() {
        return this.country;
    }

    public String getTown() {
        return this.town;
    }

    public String getNeighbourhood() {
        return this.neighbourhood;
    }

    public String getStreet() {
        return this.street;
    }

    public int getApartmentNumber() {
        return this.apartmentNumber;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public void setNeighbourhood(String neighbourhood) {
        this.neighbourhood = neighbourhood;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setApartmentNumber(int apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", country='" + country + '\'' +
                ", town='" + town + '\'' +
                ", neighbourhood='" + neighbourhood + '\'' +
                ", street='" + street + '\'' +
                ", apartmentNumber=" + apartmentNumber +
                '}';
    }
}
