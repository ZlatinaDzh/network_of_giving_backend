package com.example.demo.data;

import javax.validation.constraints.NotNull;

public class Donation {
    private int id;
    @NotNull
    private String nameDonator;
    @NotNull
    private String nameCharity;
    private int amount;
    private String date;

    public Donation(int id, String nameDonator, String nameCharity, int amount, String date) {
        this.id = id;
        this.nameDonator = nameDonator;
        this.nameCharity = nameCharity;
        this.amount = amount;
        this.date = date;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameDonator() {
        return this.nameDonator;
    }

    public void setNameDonator(String nameDonator) {
        this.nameDonator = nameDonator;
    }

    public String getNameCharity() {
        return this.nameCharity;
    }

    public void setIdCharity(String nameCharity) {
        this.nameCharity = nameCharity;
    }

    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Donation{" +
                "id=" + id +
                ", nameDonator='" + nameDonator + '\'' +
                ", nameCharity='" + nameCharity + '\'' +
                ", amount=" + amount +
                ", date='" + date + '\'' +
                '}';
    }
}
