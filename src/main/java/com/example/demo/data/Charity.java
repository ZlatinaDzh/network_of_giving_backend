package com.example.demo.data;

import javax.validation.constraints.NotNull;

public class Charity {
    @NotNull
    private String name;
    private String thumbnail;
    private String description;
    private int numberOfParticipants;
    private int requiredBudget;
    private String nameCreator;

    public Charity(String name, String thumbnail, String description, int numberOfParticipants, int requiredBudget, String nameCreator) {
        this.name = name;
        this.thumbnail = thumbnail;
        this.description = description;
        this.numberOfParticipants = numberOfParticipants;
        this.requiredBudget = requiredBudget;
        this.nameCreator = nameCreator;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumberOfParticipants() {
        return this.numberOfParticipants;
    }

    public void setNumberOfParticipants(int numberOfParticipants) {
        this.numberOfParticipants = numberOfParticipants;
    }

    public int getRequiredBudget() {
        return this.requiredBudget;
    }

    public void setRequiredBudget(int requiredBudget) {
        this.requiredBudget = requiredBudget;
    }

    public String getNameCreator() {
        return this.nameCreator;
    }

    public void setNameCreator(String nameCreator) {
        this.nameCreator = nameCreator;
    }

    public String getThumbnail() {
        return this.thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public String toString() {
        return "Charity{" +
                "name='" + name + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", description='" + description + '\'' +
                ", numberOfParticipants=" + numberOfParticipants +
                ", requiredBudget=" + requiredBudget +
                ", nameCreator='" + nameCreator + '\'' +
                '}';
    }
}
