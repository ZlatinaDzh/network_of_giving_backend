package com.example.demo.data;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MyDate {
    public static String getDateTimeAsString() {
        Date date = new Date();
        SimpleDateFormat sdf =
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
        }
}
