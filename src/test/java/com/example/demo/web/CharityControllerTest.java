package com.example.demo.web;

import com.example.demo.data.Charity;
import com.example.demo.impl.CharityManager;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = CharityController.class)
class CharityControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CharityManager charityManager;

    @Test
    public void testFetchingAllCharitiesWithValidUrlReturns200() throws Exception {
        mockMvc.perform(get("/charities")
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    public void testAddCharityWithInvalidInputReturns400() throws Exception {
        Charity charity = new Charity(null,"WORLD","save the planet",15,123,"Zlatina");
        mockMvc.perform(post("/charities")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(charity)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testAddCharityWithValidInputReturns204() throws Exception {
        Charity charity = new Charity("Green Plant","WORLD","save the planet",15,123,"Zlatina");
        mockMvc.perform(post("/charities")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(charity)))
                .andExpect(status().isOk());
    }
}