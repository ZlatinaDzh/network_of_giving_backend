package com.example.demo.web;

import com.example.demo.data.Donation;
import com.example.demo.impl.DonationManager;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = DonationController.class)
class DonationControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private DonationManager donationManager;



    @Test
    public void testAddUserWithInvalidInputParameterCharityNameReturns400() throws Exception {
        Donation donation = new Donation(15,"Zlati", null,125,"1978-12-03") ;
        mockMvc.perform(post("/donations")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(donation)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testAddUserWithInvalidInputParameterDonatorNameNameReturns400() throws Exception {
        Donation donation = new Donation(15,null, "world",125,"1978-12-03");
        mockMvc.perform(post("/donations")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(donation)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testAddUserWithInvalidInputParameterDonatorAndCharitNameNameNameReturns400() throws Exception {
        Donation donation = new Donation(15,null, null,125,"1978-12-03");
        mockMvc.perform(post("/donations")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(donation)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testAddUserWithValidInputReturns204() throws Exception {
        Donation donation = new Donation(15,"Zlati", "world",125,"1978-12-03");
        mockMvc.perform(post("/donations")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(donation)))
                .andExpect(status().isNoContent());
    }
}