package com.example.demo.web;

import com.example.demo.data.Address;
import com.example.demo.impl.AddressManager;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = AddressController.class)
class AddressControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private AddressManager addressManagerManager;

    @Test
    public void testFetchingAllAddressesWithValidUrlReturns200() throws Exception {
        mockMvc.perform(get("/addresses")
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    public void testAddAddressWithInvalidInputReturns400() throws Exception {
        Address address = new Address(0,null, "Burgas", "Izgrev", "Win",25);
        mockMvc.perform(post("/addresses")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(address)))
                .andExpect(status().isBadRequest());
    }


}