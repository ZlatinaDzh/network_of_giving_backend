package com.example.demo.web;

import com.example.demo.data.Gender;
import com.example.demo.impl.UserManager;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = UserController.class)
class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserManager userManager;

    @Test
    public void testFetchingAllUsersWithValidUrlReturns200() throws Exception {
        mockMvc.perform(get("/users")
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    public void testAddUserWithInvalidInputReturns400() throws Exception {
        UserDTO user = new UserDTO();
        mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testAddUserWithValidInputReturns204() throws Exception {
        UserDTO user = new UserDTO("Zlatina", "Dzhanavarova", "Zlati", "parolata", 21, Gender.FEMALE, "Bulgaria",
                                   "Burgas", "Zortnitsa", "Secret", 32);
        mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isOk());
    }

//    @Test
//    public void testGetUserReturnsAddress() throws Exception {
//        User expect = new User("Zlatina", "Dzhanavarova", "Zlati", "parolata", 21, Gender.FEMALE, 0);
//        MvcResult mvcResult = mockMvc.perform(get("/users/Zlati")
//                .contentType("application/json")
//                .param("userName", "Zlati")).andReturn();
//        String actualResponseBody = mvcResult.getResponse().getContentAsString();
//        assertThat(objectMapper.writeValueAsString(expect))
//                .isEqualToIgnoringWhitespace(actualResponseBody);
//    }
}