package com.example.demo.web;

import com.example.demo.data.Participation;
import com.example.demo.impl.ParticipationManager;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = ParticipationController.class)
class ParticipationControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ParticipationManager donationManager;

    

    @Test
    public void testAddUserWithInvalidInputParameterCharityNameReturns400() throws Exception {
        Participation participation = new Participation(15,"Zlati", null,"1978-12-03") ;
        mockMvc.perform(post("/participations")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(participation)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testAddUserWithInvalidInputParameterDonatorNameNameReturns400() throws Exception {
        Participation participation = new Participation(15,null, "world","1978-12-03");
        mockMvc.perform(post("/participations")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(participation)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testAddUserWithInvalidInputParameterDonatorAndCharitNameNameNameReturns400() throws Exception {
       Participation participation = new Participation(15,null, null,"1978-12-03");
        mockMvc.perform(post("/participations")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(participation)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testAddUserWithValidInputReturns204() throws Exception {
        Participation participation = new Participation(15,"Zlati", "world","1978-12-03");
        mockMvc.perform(post("/participations")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(participation)))
                .andExpect(status().isNoContent());
    }

}